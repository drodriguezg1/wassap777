package com.example.chattt.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chattt.chat.ChatMessage;
import com.example.chattt.databinding.ItemContainerRecivedMessageBinding;
import com.example.chattt.databinding.ItemContainerSentMessageBinding;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private  Bitmap reciverProfileImage;
    private final List<ChatMessage> chatMessages;
    private final String senderId;

    public static final int VIEW_TYPE_SENT = 1;
    public static final int VIEW_TYPE_RECIVED = 2;

    public void setRevierProfileImage(Bitmap reciverProfileImage) {
        this.reciverProfileImage = reciverProfileImage;
    }

    public ChatAdapter(List<ChatMessage> chatMessages, Bitmap reciverProfileImage, String senderId) {
        this.chatMessages = chatMessages;
        this.reciverProfileImage = reciverProfileImage;
        this.senderId = senderId;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==VIEW_TYPE_SENT)
            return new SentMessageViewHolder(ItemContainerSentMessageBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
        else  {
            return new RecivedMessageViewHolder(ItemContainerRecivedMessageBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position)==VIEW_TYPE_SENT)
            ((SentMessageViewHolder)holder).setData(chatMessages.get(position));
        else
            ((RecivedMessageViewHolder)holder).setData(chatMessages.get(position),reciverProfileImage);
    }

    @Override
    public int getItemCount() {
        return chatMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (chatMessages.get(position).senderId.equals(senderId)) {
            return VIEW_TYPE_SENT;
        }
        else {
            return VIEW_TYPE_RECIVED;
        }
    }

    static class SentMessageViewHolder extends RecyclerView.ViewHolder {
        private final ItemContainerSentMessageBinding itemContainerSentMessageBinding;

        SentMessageViewHolder(ItemContainerSentMessageBinding itemContainerSentMessageBinding) {
            super(itemContainerSentMessageBinding.getRoot());
            this.itemContainerSentMessageBinding = itemContainerSentMessageBinding;
        }

        void setData(ChatMessage chatMessage) {
            itemContainerSentMessageBinding.textMessage.setText(chatMessage.message);
            itemContainerSentMessageBinding.textDateTime.setText(chatMessage.dateTime);
        }
    }

    static class RecivedMessageViewHolder extends RecyclerView.ViewHolder {
        private final ItemContainerRecivedMessageBinding itemContainerRecivedMessageBinding;

        RecivedMessageViewHolder(ItemContainerRecivedMessageBinding itemContainerRecivedMessageBinding) {
            super(itemContainerRecivedMessageBinding.getRoot());
            this.itemContainerRecivedMessageBinding = itemContainerRecivedMessageBinding;
        }

        void setData(ChatMessage chatMessage, Bitmap reciverProfileImage) {

            itemContainerRecivedMessageBinding.textMessage.setText(chatMessage.message);
            itemContainerRecivedMessageBinding.textDateTime.setText(chatMessage.dateTime);
            if (reciverProfileImage!=null){
                itemContainerRecivedMessageBinding.imageProfile.setImageBitmap(reciverProfileImage);

            }
        }
        private Bitmap getConversionImage(String encodesImage){
            byte[] bytes = android.util.Base64.decode(encodesImage, android.util.Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }
    }
}
