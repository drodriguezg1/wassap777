package com.example.chattt.network;

import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class ApiClient {
    private static final String BASE_URL = "https://fcm.googleapis.com/fcm/";
    private static Retrofit retrofit;
    public static Retrofit getClient(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
//    public static <S> S buildService(Class<S> serviceType){
//        return getClient().create(serviceType);
//    }
}
