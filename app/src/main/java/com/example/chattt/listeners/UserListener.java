package com.example.chattt.listeners;

import com.example.chattt.models.User;

public interface UserListener {
    void onUserClicked(User user);
}
