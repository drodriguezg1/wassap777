package com.example.chattt.listeners;

import com.example.chattt.models.User;

public interface ConversionListener {
    void onConversionClicked(User user);
}
